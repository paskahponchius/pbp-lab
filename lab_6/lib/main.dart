import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
// import 'package:flutter_app_learning/NavDrawer.dart';
// import 'package:flutter_app_learning/contact.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Learning',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() {
    return _MyHomePageState();
  }
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Swab.In Article"),
      ),
      body: Card(
        child: ListView(children: <Widget>[
          Container(
            height: 180,
            color: Colors.white,
            child: Row(
              children: [
                Center(
                  child: Padding(
                    padding: EdgeInsets.all(10),
                    child: Expanded(
                      child: Image.asset('assets/images/swab_test.jpg'),
                      flex: 2,
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    alignment: Alignment.topLeft,
                    child: Column(
                      children: [
                        Expanded(
                          flex: 5,
                          child: ListTile(
                            title: Text(
                                "Apa Itu Rapid Test, Swab, dan PCR, apakah tahu Perbedaannya?"),
                            // subtitle: Text("Ed She"),
                          ),
                        ),
                        Expanded(
                          flex: 5,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              TextButton(
                                child: Text("Selengkapnya"),
                                onPressed: () {},
                              ),
                              SizedBox(
                                width: 8,
                              ),
                              // w
                              SizedBox(
                                width: 8,
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  flex: 8,
                ),
              ],
            ),
          ),
          const SizedBox(height: 10),
          Container(
            height: 180,
            color: Colors.grey,
            child: Row(
              children: [
                Center(
                  child: Padding(
                    padding: EdgeInsets.all(10),
                    child: Expanded(
                      child: Image.asset('assets/images/vaksin.jpg'),
                      flex: 2,
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    alignment: Alignment.topLeft,
                    child: Column(
                      children: [
                        Expanded(
                          flex: 5,
                          child: ListTile(
                            title: Text(
                                "Perbedaan 9 jenis vaksin Covid-19 di Indonesia, mulai Sinovac hingga Convidencia"),
                            // subtitle: Text("Ed She"),
                          ),
                        ),
                        Expanded(
                          flex: 5,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              TextButton(
                                child: Text("Selengkapnya"),
                                onPressed: () {},
                              ),
                              SizedBox(
                                width: 8,
                              ),
                              // w
                              SizedBox(
                                width: 8,
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  flex: 8,
                ),
              ],
            ),
          ),
          const SizedBox(height: 10),
          Container(
            height: 180,
            color: Colors.white,
            child: Row(
              children: [
                Center(
                  child: Padding(
                    padding: EdgeInsets.all(10),
                    child: Expanded(
                      child: Image.asset('assets/images/covid.jpg'),
                      flex: 2,
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    alignment: Alignment.topLeft,
                    child: Column(
                      children: [
                        Expanded(
                          flex: 5,
                          child: ListTile(
                            title: Text(
                                "Alasan Pentingnya Vaksinasi dan Efektivitasnya dalam Memutus Penularan COVID-19"),
                            // subtitle: Text("Ed She"),
                          ),
                        ),
                        Expanded(
                          flex: 5,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              TextButton(
                                child: Text("Selengkapnya"),
                                onPressed: () {},
                              ),
                              SizedBox(
                                width: 8,
                              ),
                              // w
                              SizedBox(
                                width: 8,
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  flex: 8,
                ),
              ],
            ),
          ),
          const SizedBox(height: 10),
          Container(
            height: 180,
            color: Colors.grey,
            child: Row(
              children: [
                Center(
                  child: Padding(
                    padding: EdgeInsets.all(10),
                    child: Expanded(
                      child: Image.asset('assets/images/botol_vaksin.jpg'),
                      flex: 2,
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    alignment: Alignment.topLeft,
                    child: Column(
                      children: [
                        Expanded(
                          flex: 5,
                          child: ListTile(
                            title: Text(
                                "Jangan Remehkan! Ini Pentingnya Tes Dalam Bendung Covid-19"),
                            // subtitle: Text("Ed She"),
                          ),
                        ),
                        Expanded(
                          flex: 5,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              TextButton(
                                child: Text("Selengkapnya"),
                                onPressed: () {},
                              ),
                              SizedBox(
                                width: 8,
                              ),
                              // w
                              SizedBox(
                                width: 8,
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  flex: 8,
                ),
              ],
            ),
          ),
        ]),
        elevation: 8,
        margin: EdgeInsets.all(10),
      ),
    );
  }
}
