from django.db import models

class Note(models.Model):
    To = models.CharField(max_length=30)
    From = models.CharField(max_length=30)
    Title = models.CharField(max_length=30)
    Message = models.CharField(max_length=100)
# Create your models here.