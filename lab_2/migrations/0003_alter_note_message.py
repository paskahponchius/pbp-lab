# Generated by Django 3.2.7 on 2021-10-10 19:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_2', '0002_auto_20210925_0121'),
    ]

    operations = [
        migrations.AlterField(
            model_name='note',
            name='Message',
            field=models.TextField(),
        ),
    ]
